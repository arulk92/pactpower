
<?php include_once 'header.php';?>
    
    <section id="page-breadcrumb">
        <div class="vertical-center sun">
             <div class="container">
                <div class="row">
                    <div class="action">
                        <div class="col-sm-12">
                            <h1 class="title">Our Business</h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--/#page-breadcrumb-->
    
    <section id="blog" class="padding-bottom">
        <div class="container">
            <div class="row">
                <div class="timeline-blog overflow padding-top">
                   <!-- <div class="timeline-date text-center">
                        <a href="#" class="btn btn-common uppercase">November 2013</a>
                    </div>-->
                    <div class="timeline-divider overflow padding-bottom">
                        <div class="col-sm-6 padding-right arrow-right wow fadeInLeft" data-wow-duration="1000ms" data-wow-delay="300ms">
                            <div class="single-blog timeline">
                                <div class="single-blog-wrapper">
                                    <div class="post-thumb">
                                        <img src="images/blog/timeline/2.jpg" class="img-responsive" alt="">

                                    </div>
                                </div>
                                <div class="post-content overflow">
                                    <!-- <h2 class="post-title bold"><a href="blogdetails.html#">Advanced business cards design</a></h2>-->
                                    <h5 style="text-align: justify;"><strong>PACT Power is the authorized dealer of Volvo Penta in Tamil Nadu & Pondicherry.</strong></h5>

                                    <p style="text-align: justify;">Volvo is one of the world’s largest producer of trucks, buses, construction equipment, engines & holds a leading position in in the fields of marine & industrial power systems & aircraft engine components. It is associated with innovative & performance-oriented products in addition to quality, safety & environment.</p>

                                    <p style="text-align: justify;">Volvo believes in its core values - quality, safety & environmental care. With a wide network of state of the art service & training facilities throughout the world, Volvo offers innovative ways to keep dealers, service professionals & customers up-to-date with the latest expert, hands-on know-how</p>

                                    <div class="post-bottom overflow">

                                    </div>
                                </div>
                            </div>
                        </div>



                        <div class="col-sm-6 padding-left padding-top arrow-left wow fadeInRight" data-wow-duration="1000ms" data-wow-delay="300ms">
                            <div class="single-blog timeline">
                                <div class="single-blog-wrapper">
                                    <div class="post-thumb">
                                        <img src="images/blog/timeline/1.jpg" class="img-responsive" alt="">
                                        <!-- <div class="post-overlay">
                                            <span class="uppercase"><a href="#">14 <br><small>Feb</small></a></span>
                                        </div>-->
                                    </div>
                                </div>
                                <div class="post-content overflow">
                                    <!-- <h2 class="post-title bold"><a href="#">Advanced business cards design</a></h2>-->
                                    <h5 style="text-align: justify;"><strong>PACT Power is the authorized dealer of Linde Material Handling Equipment.</strong></h5>
                                    <p style="text-align: justify;">Linde Material Handling GmbH, (www.linde-mh.de) founded in 1904 under the
                                        name of Guldner Morten-Gesellschaft is headquartered in Ashaffenburg,
                                        Germany, Linde ranks amongst the world's foremost maker of fork-lift trucks and
                                        warehouse handling equipment. Linde brand stands for technological and
                                        innovative leadership and offers technically ambitious solutions enhancing
                                        efficiency and excellent design, also providing comprehensive service
                                        skills across the globe.</p>

                                    <p style="text-align: justify;">Linde has been part of the KION Group since 2006.</p>

                                    <p style="text-align: justify;">Linde offers high performance solutions for intralogistics. The basis for this is electric & diesel forklifts, warehouse equipment, fleet management software, automation solutions, driver assistance systems as well as services & operator training.</p>

                                    <p style="text-align: justify;">Linde’s vehicle offer is unique. It comprises 77 series with upto 382 model variants & around 10,000 equipment options. On the basis of this modular system, Linde manufactures the vehicles & fleet precisely tailored to their requirements for transport, picking & stacking.</p>
                                    <div class="post-bottom overflow">

                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>


            </div>
        </div>
    </section>
    <!--/#blog-->

<?php include_once 'footer.php';?>