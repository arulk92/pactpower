<?php include_once 'header.php';?>


<section id="page-breadcrumb">
    <div class="vertical-center sun">
        <div class="container">
            <div class="row">
                <div class="action">
                    <div class="col-sm-12">
                        <h1 class="title">Marine Engines</h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<section id="company-information" class="choose">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 wow fadeInDown animated" data-wow-duration="1000ms" data-wow-delay="0ms" style="visibility: visible; animation-duration: 1000ms; animation-delay: 0ms; animation-name: fadeInDown;">
                <img src="images/portfolio/marine-leisure.jpg" class="img-responsive" alt="">
            </div>
            <div class="col-sm-6 padding-top wow fadeInDown animated" data-wow-duration="1000ms" data-wow-delay="0ms" style="visibility: visible; animation-duration: 1000ms; animation-delay: 0ms; animation-name: fadeInDown;">
                <strong>Marine Commercial</strong>
                <p> Engine Range - 286 to 753 HP</p>
               <strong>Marine Leisure</strong>
                <p>Engine Range - 249 to 942 HP</p>
                <p>Major Applications where Volvo Penta marine engines are used : </p>
                <ul class="elements">
                    <li class="wow fadeInUp animated" data-wow-duration="900ms" data-wow-delay="100ms" style="visibility: visible; animation-duration: 900ms; animation-delay: 100ms; animation-name: fadeInUp;"><i class="fa fa-angle-right"></i> Barges</li>
                    <li class="wow fadeInUp animated" data-wow-duration="800ms" data-wow-delay="200ms" style="visibility: visible; animation-duration: 800ms; animation-delay: 200ms; animation-name: fadeInUp;"><i class="fa fa-angle-right"></i> Fast Patrol Boats</li>
                    <li class="wow fadeInUp animated" data-wow-duration="700ms" data-wow-delay="300ms" style="visibility: visible; animation-duration: 700ms; animation-delay: 300ms; animation-name: fadeInUp;"><i class="fa fa-angle-right"></i> Survey Boats</li>
                    <li class="wow fadeInUp animated" data-wow-duration="600ms" data-wow-delay="400ms" style="visibility: visible; animation-duration: 600ms; animation-delay: 400ms; animation-name: fadeInUp;"><i class="fa fa-angle-right"></i> Rigid Inflatable boats</li>
                    <li class="wow fadeInUp animated" data-wow-duration="500ms" data-wow-delay="500ms" style="visibility: visible; animation-duration: 500ms; animation-delay: 500ms; animation-name: fadeInUp;"><i class="fa fa-angle-right"></i> Rescue Boats</li>
                    <li class="wow fadeInUp animated" data-wow-duration="500ms" data-wow-delay="500ms" style="visibility: visible; animation-duration: 500ms; animation-delay: 500ms; animation-name: fadeInUp;"><i class="fa fa-angle-right"></i> Offshore supply vessel</li>
                    <li class="wow fadeInUp animated" data-wow-duration="500ms" data-wow-delay="500ms" style="visibility: visible; animation-duration: 500ms; animation-delay: 500ms; animation-name: fadeInUp;"><i class="fa fa-angle-right"></i> Parasailing boats</li>
                    <li class="wow fadeInUp animated" data-wow-duration="500ms" data-wow-delay="500ms" style="visibility: visible; animation-duration: 500ms; animation-delay: 500ms; animation-name: fadeInUp;"><i class="fa fa-angle-right"></i> Fishing & Trawling</li>
                    <li class="wow fadeInUp animated" data-wow-duration="500ms" data-wow-delay="500ms" style="visibility: visible; animation-duration: 500ms; animation-delay: 500ms; animation-name: fadeInUp;"><i class="fa fa-angle-right"></i> Auxilliary & Emergency gensets</li>
                </ul>
            </div>
        </div>
<br><br>
        <div class="row">
            <div class="col-sm-6 padding-top wow fadeInDown animated" data-wow-duration="1000ms" data-wow-delay="0ms" style="visibility: visible; animation-duration: 1000ms; animation-delay: 0ms; animation-name: fadeInDown;">
                <p style="font: bold">Engine & Drive Configurations : </p>
                <ul class="elements">
                    <li class="wow fadeInUp animated" data-wow-duration="900ms" data-wow-delay="100ms" style="visibility: visible; animation-duration: 900ms; animation-delay: 100ms; animation-name: fadeInUp;"><i class="fa fa-angle-right"></i> Inboard Engine</li>
                    <li class="wow fadeInUp animated" data-wow-duration="800ms" data-wow-delay="200ms" style="visibility: visible; animation-duration: 800ms; animation-delay: 200ms; animation-name: fadeInUp;"><i class="fa fa-angle-right"></i> Conventional gearbox design</li>
                    <li class="wow fadeInUp animated" data-wow-duration="700ms" data-wow-delay="300ms" style="visibility: visible; animation-duration: 700ms; animation-delay: 300ms; animation-name: fadeInUp;"><i class="fa fa-angle-right"></i> Diesel Aquamatic Sterndrive</li>
                    <li class="wow fadeInUp animated" data-wow-duration="600ms" data-wow-delay="400ms" style="visibility: visible; animation-duration: 600ms; animation-delay: 400ms; animation-name: fadeInUp;"><i class="fa fa-angle-right"></i> IPS - Inboard Performance System</li>
                </ul>



            </div>
            <div class="col-sm-6 wow fadeInDown animated" data-wow-duration="1000ms" data-wow-delay="0ms" style="visibility: visible; animation-duration: 1000ms; animation-delay: 0ms; animation-name: fadeInDown;">
                <strong>Diesel Aquamatic Sterndrive </strong><br><br>
                <img src="images/portfolio/diesel-aquamatic.jpg" class="img-responsive" alt="">
            </div>

        </div>
    </div>
</section>



<?php include_once 'footer.php';?>
