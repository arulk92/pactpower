<footer id="footer">
    <div class="container">
        <div class="row">
           <!-- <div class="col-sm-12 text-center bottom-separator">
                <img src="images/home/under.png" class="img-responsive inline" alt="">
            </div>
           -->
            <div class="col-sm-12">
                <div class="copyright-text text-center">
                    <p>&copy; PACT Power Solutions LLP 2018. All Rights Reserved.</p>
                   <!-- <p>Designed by <a target="_blank" href="http://www.themeum.com">Themeum</a></p>-->
                </div>
            </div>
        </div>
    </div>
</footer>
<!--/#footer-->

<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/lightbox.min.js"></script>
<script type="text/javascript" src="js/wow.min.js"></script>
<script type="text/javascript" src="js/main.js"></script>
</body>
</html>
