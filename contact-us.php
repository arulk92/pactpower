<?php include_once 'header.php';?>

<section id="page-breadcrumb" xmlns="http://www.w3.org/1999/html">
        <div class="vertical-center sun">
             <div class="container">
                <div class="row">
                    <div class="action">
                        <div class="col-sm-12">
                            <h1 class="title">Contact US</h1>
                        </div>                        
                    </div>
                </div>
            </div>
        </div>
   </section>
    <!--/#action-->

    <section id="map-section">
        <div class="container">
            <!--<div id="gmap"></div>-->

            <div><iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15547.86963572266!2d80.0246588!3d13.0377465!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x2dfdb91485f66cbc!2sPACT+Power+Solutions+LLP!5e0!3m2!1sen!2sin!4v1546255695117" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe></div>
            <div class="contact-info">
                <h2 style="color: #7EBC3F;">Contacts</h2>
                <address>

                Phone: +91 - 98844 52425<br>

                E-mail: <a href="mailto:enquiry@pactpower.in">enquiry@pactpower.in</a> <br><br>

                    <strong>Service Locations</strong>: Chennai,Coimbatore,Madurai.<br><b>Spares Locations</b>: Chennai,Coimbatore,Karur,Salem,
                    Madurai,Tirunelveli.

                </address>

                <h2 style="color: #7EBC3F;">Head Office</h2>
                <address>
                    <strong>PACT Power Solutions LLP</strong><br>
                    No. 5/55, Kuthambakkam Village & Post  <br>
                    Forest Range Road, Chettipedu<br>
                    Poonamallee TK, Thiruvallur Dist<br>
                    Chennai - 600 124,<br>
                    Tamil Nadu, India.<br>
                </address>
            </div>
        </div>
    </section> <!--/#map-section-->        

    <?php include_once 'footer.php';?>

    <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script>
    <script type="text/javascript" src="js/gmaps.js"></script>

