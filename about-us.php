<?php include_once 'header.php';?>

    <section id="page-breadcrumb">
        <div class="vertical-center sun">
             <div class="container">
                <div class="row">
                    <div class="action">
                        <div class="col-sm-12">
                            <h1 class="title">About Us</h1>
                            <p>Why our Clients love to work with us.</p>
                        </div>
                     </div>
                </div>
            </div>
        </div>
   </section>
    <!--/#page-breadcrumb-->

    <section id="company-information" class="padding wow fadeIn" data-wow-duration="1000ms" data-wow-delay="300ms">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <img src="images/aboutus/abt-us.jpg" class="img-responsive" alt="">
                </div>
                <div class="col-sm-6 padding-top">
                    <p style="text-align: justify;">PACT Power Solutions LLP (PPS), is a Subsidiary of Advanced Construction Technologies Pvt Ltd (ACT). ACT is a pioneer in bringing innovative and cutting-edge technology in the form of products and services to deliver superior productivity, efficiency and safety to India, with more than 30 years of excellence in keeping up with this mission. PACT Power Solutions (PPS) was incorporated in 2016 to bring focus on new market segments, customers and product lines, through authorised dealership of 2 world renowned brands VOLVO PENTA (Part of VOLVO Group) and LINDE (Part of KION Group).</p>

                </div>
            </div>
        </div>
    </section>



<?php include_once 'footer.php';?>