
<?php include_once 'header.php';?>


    <section id="page-breadcrumb">
        <div class="vertical-center sun">
             <div class="container">
                <div class="row">
                    <div class="action">
                        <div class="col-sm-12">
                            <h1 class="title">Products</h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
   </section>
    <!--/#action-->

    <section id="portfolio">
        <div class="container">
            <div class="row">
                <ul class="portfolio-filter text-center">
                    <li><a class="btn btn-default active" href="#" data-filter="*">All</a></li>
                    <li><a class="btn btn-default " href="#" data-filter=".industrial">Industrial</a></li>
                    <li><a class="btn btn-default" href="#" data-filter=".marine">Marine</a></li>
                    <li><a class="btn btn-default" href="#" data-filter=".forklifts">Forklifts</a></li>
                    <li><a class="btn btn-default" href="#" data-filter=".bopt">BOPT</a></li>
                    <li><a class="btn btn-default" href="#" data-filter=".stacker">Stacker</a></li>
                    <li><a class="btn btn-default" href="#" data-filter=".reach">Reach Truck</a></li>
                    <li><a class="btn btn-default" href="#" data-filter=".tow">Tow Truck</a></li>
                    <li><a class="btn btn-default" href="#" data-filter=".order">Order Picker</a></li>
                    <li><a class="btn btn-default" href="#" data-filter=".hand">Hand Pallet Truck</a></li>
                </ul><!--/#portfolio-filter-->
                    
                <div class="portfolio-items">
                    <div class="col-xs-6 col-sm-4 col-md-3 portfolio-item branded industrial">
                        <div class="portfolio-wrapper">
                            <div class="portfolio-single">
                                <div class="portfolio-thumb">
                                    <img src="images/products/industrial.jpg" class="img-responsive" alt="">
                                </div>
                                <div class="portfolio-view">
                                    <ul class="nav nav-pills">
                                        <li><a href="images/products/industrial.jpg" data-lightbox="example-set"><i class="fa fa-eye"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                            <!--<div class="portfolio-info ">
                                <h2>Sailing Vivamus</h2>
                            </div>-->
                        </div>
                    </div>

                    <div class="col-xs-6 col-sm-4 col-md-3 portfolio-item branded marine">
                        <div class="portfolio-wrapper">
                            <div class="portfolio-single">
                                <div class="portfolio-thumb">
                                    <img src="images/products/marine.jpg" class="img-responsive" alt="">
                                </div>
                                <div class="portfolio-view">
                                    <ul class="nav nav-pills">
                                        <li><a href="images/products/marine.jpg" data-lightbox="example-set"><i class="fa fa-eye"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                            <!--<div class="portfolio-info ">
                                <h2>Sailing Vivamus</h2>
                            </div>-->
                        </div>
                    </div>

                    <div class="col-xs-6 col-sm-4 col-md-3 portfolio-item branded forklifts">
                        <div class="portfolio-wrapper">
                            <div class="portfolio-single">
                                <div class="portfolio-thumb">
                                    <img src="images/products/fork.jpg" class="img-responsive" alt="">
                                </div>
                                <div class="portfolio-view">
                                    <ul class="nav nav-pills">
                                        <li><a href="images/products/fork.jpg" data-lightbox="example-set"><i class="fa fa-eye"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                            <!--<div class="portfolio-info ">
                                <h2>Sailing Vivamus</h2>
                            </div>-->
                        </div>
                    </div>


                    <div class="col-xs-6 col-sm-4 col-md-3 portfolio-item branded bopt">
                        <div class="portfolio-wrapper">
                            <div class="portfolio-single">
                                <div class="portfolio-thumb">
                                    <img src="images/products/bopt.jpg" class="img-responsive" alt="">
                                </div>
                                <div class="portfolio-view">
                                    <ul class="nav nav-pills">
                                        <li><a href="images/products/bopt.jpg" data-lightbox="example-set"><i class="fa fa-eye"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                           <!-- <div class="portfolio-info ">
                                <h2>Sailing Vivamus</h2>
                            </div>-->
                        </div>
                    </div>

                    <div class="col-xs-6 col-sm-4 col-md-3 portfolio-item branded stacker">
                        <div class="portfolio-wrapper">
                            <div class="portfolio-single">
                                <div class="portfolio-thumb">
                                    <img src="images/products/stacker.jpg" class="img-responsive" alt="">
                                </div>
                                <div class="portfolio-view">
                                    <ul class="nav nav-pills">
                                        <li><a href="images/products/stacker.jpg" data-lightbox="example-set"><i class="fa fa-eye"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                           <!-- <div class="portfolio-info ">
                                <h2>Sailing Vivamus</h2>
                            </div>-->
                        </div>
                    </div>

                    <div class="col-xs-6 col-sm-4 col-md-3 portfolio-item branded reach">
                        <div class="portfolio-wrapper">
                            <div class="portfolio-single">
                                <div class="portfolio-thumb">
                                    <img src="images/products/reach.jpg" class="img-responsive" alt="">
                                </div>
                                <div class="portfolio-view">
                                    <ul class="nav nav-pills">
                                        <li><a href="images/products/reach.jpg" data-lightbox="example-set"><i class="fa fa-eye"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                            <!--<div class="portfolio-info ">
                                <h2>Sailing Vivamus</h2>
                            </div>-->
                        </div>
                    </div>

                    <div class="col-xs-6 col-sm-4 col-md-3 portfolio-item branded tow">
                        <div class="portfolio-wrapper">
                            <div class="portfolio-single">
                                <div class="portfolio-thumb">
                                    <img src="images/products/tow.jpg" class="img-responsive" alt="">
                                </div>
                                <div class="portfolio-view">
                                    <ul class="nav nav-pills">
                                        <li><a href="images/products/tow.jpg" data-lightbox="example-set"><i class="fa fa-eye"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                            <!--<div class="portfolio-info ">
                                <h2>Sailing Vivamus</h2>
                            </div>-->
                        </div>
                    </div>


                    <div class="col-xs-6 col-sm-4 col-md-3 portfolio-item branded order">
                        <div class="portfolio-wrapper">
                            <div class="portfolio-single">
                                <div class="portfolio-thumb">
                                    <img src="images/products/order.jpg" class="img-responsive" alt="">
                                </div>
                                <div class="portfolio-view">
                                    <ul class="nav nav-pills">
                                        <li><a href="images/products/order.jpg" data-lightbox="example-set"><i class="fa fa-eye"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                            <!--<div class="portfolio-info ">
                                <h2>Sailing Vivamus</h2>
                            </div>-->
                        </div>
                    </div>

                    <div class="col-xs-6 col-sm-4 col-md-3 portfolio-item branded hand">
                        <div class="portfolio-wrapper">
                            <div class="portfolio-single">
                                <div class="portfolio-thumb">
                                    <img src="images/products/hand.jpg" class="img-responsive" alt="">
                                </div>
                                <div class="portfolio-view">
                                    <ul class="nav nav-pills">
                                        <li><a href="images/products/hand.jpg" data-lightbox="example-set"><i class="fa fa-eye"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                           <!-- <div class="portfolio-info ">
                                <h2>Sailing Vivamus</h2>
                            </div>-->
                        </div>
                    </div>




                </div>

            </div>
        </div>
    </section>
    <!--/#portfolio-->


    <?php include_once 'footer.php';?>
    <script type="text/javascript" src="js/jquery.isotope.min.js"></script>
