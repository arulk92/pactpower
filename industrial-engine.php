<?php include_once 'header.php';?>


<section id="page-breadcrumb">
    <div class="vertical-center sun">
        <div class="container">
            <div class="row">
                <div class="action">
                    <div class="col-sm-12">
                        <h1 class="title">Industrial Engine</h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<section id="company-information" class="choose">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 wow fadeInDown animated" data-wow-duration="1000ms" data-wow-delay="0ms" style="visibility: visible; animation-duration: 1000ms; animation-delay: 0ms; animation-name: fadeInDown;">
                <img src="images/portfolio/power-generator.jpg" class="img-responsive" alt="">
            </div>
            <div class="col-sm-6 padding-top wow fadeInDown animated" data-wow-duration="1000ms" data-wow-delay="0ms" style="visibility: visible; animation-duration: 1000ms; animation-delay: 0ms; animation-name: fadeInDown;">
                <h2>Power Generation</h2>
                <p>Genset Range : 250 to 650 KVa</p>
                <ul class="elements">
                    <li class="wow fadeInUp animated" data-wow-duration="900ms" data-wow-delay="100ms" style="visibility: visible; animation-duration: 900ms; animation-delay: 100ms; animation-name: fadeInUp;"><i class="fa fa-angle-right"></i> Powerful Engine</li>
                    <li class="wow fadeInUp animated" data-wow-duration="800ms" data-wow-delay="200ms" style="visibility: visible; animation-duration: 800ms; animation-delay: 200ms; animation-name: fadeInUp;"><i class="fa fa-angle-right"></i> Unmatched Fuel Efficiency due to Electronic Engine Management.</li>
                </ul>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6 padding-top wow fadeInDown animated" data-wow-duration="1000ms" data-wow-delay="0ms" style="visibility: visible; animation-duration: 1000ms; animation-delay: 0ms; animation-name: fadeInDown;">
                <h2>Off-road and Stationary</h2>
                <p>Engine Range : 143 to 700 HP</p>
                <p>Major Applications where Volvo Penta off road & stationary engines are used : </p>
                <ul class="elements">
                    <li class="wow fadeInUp animated" data-wow-duration="900ms" data-wow-delay="100ms" style="visibility: visible; animation-duration: 900ms; animation-delay: 100ms; animation-name: fadeInUp;"><i class="fa fa-angle-right"></i> Construction Sites</li>
                    <li class="wow fadeInUp animated" data-wow-duration="800ms" data-wow-delay="200ms" style="visibility: visible; animation-duration: 800ms; animation-delay: 200ms; animation-name: fadeInUp;"><i class="fa fa-angle-right"></i> Road Building Projects</li>
                    <li class="wow fadeInUp animated" data-wow-duration="700ms" data-wow-delay="300ms" style="visibility: visible; animation-duration: 700ms; animation-delay: 300ms; animation-name: fadeInUp;"><i class="fa fa-angle-right"></i> Ports</li>
                    <li class="wow fadeInUp animated" data-wow-duration="600ms" data-wow-delay="400ms" style="visibility: visible; animation-duration: 600ms; animation-delay: 400ms; animation-name: fadeInUp;"><i class="fa fa-angle-right"></i> Freight Stations</li>
                    <li class="wow fadeInUp animated" data-wow-duration="500ms" data-wow-delay="500ms" style="visibility: visible; animation-duration: 500ms; animation-delay: 500ms; animation-name: fadeInUp;"><i class="fa fa-angle-right"></i> Onshore & Offshore Drilling</li>
                    <li class="wow fadeInUp animated" data-wow-duration="500ms" data-wow-delay="500ms" style="visibility: visible; animation-duration: 500ms; animation-delay: 500ms; animation-name: fadeInUp;"><i class="fa fa-angle-right"></i> Quarry Equipment</li>
                    <li class="wow fadeInUp animated" data-wow-duration="500ms" data-wow-delay="500ms" style="visibility: visible; animation-duration: 500ms; animation-delay: 500ms; animation-name: fadeInUp;"><i class="fa fa-angle-right"></i> Industrial Pumps</li>
                    <li class="wow fadeInUp animated" data-wow-duration="500ms" data-wow-delay="500ms" style="visibility: visible; animation-duration: 500ms; animation-delay: 500ms; animation-name: fadeInUp;"><i class="fa fa-angle-right"></i> Compressors</li>
                    <li class="wow fadeInUp animated" data-wow-duration="500ms" data-wow-delay="500ms" style="visibility: visible; animation-duration: 500ms; animation-delay: 500ms; animation-name: fadeInUp;"><i class="fa fa-angle-right"></i> Cranes</li>
                </ul>
<br>
                <p>Volvo Penta engines are well known for : </p>

                <ul class="elements">
                    <li class="wow fadeInUp animated" data-wow-duration="900ms" data-wow-delay="100ms" style="visibility: visible; animation-duration: 900ms; animation-delay: 100ms; animation-name: fadeInUp;"><i class="fa fa-angle-right"></i> High Reliability</li>
                    <li class="wow fadeInUp animated" data-wow-duration="800ms" data-wow-delay="200ms" style="visibility: visible; animation-duration: 800ms; animation-delay: 200ms; animation-name: fadeInUp;"><i class="fa fa-angle-right"></i> Superior fuel efficiency</li>
                    <li class="wow fadeInUp animated" data-wow-duration="700ms" data-wow-delay="300ms" style="visibility: visible; animation-duration: 700ms; animation-delay: 300ms; animation-name: fadeInUp;"><i class="fa fa-angle-right"></i> Environment friendly design</li>
                    <li class="wow fadeInUp animated" data-wow-duration="600ms" data-wow-delay="400ms" style="visibility: visible; animation-duration: 600ms; animation-delay: 400ms; animation-name: fadeInUp;"><i class="fa fa-angle-right"></i> Unmatched performance</li>
                </ul>

            </div>
            <div class="col-sm-6 wow fadeInDown animated" data-wow-duration="1000ms" data-wow-delay="0ms" style="visibility: visible; animation-duration: 1000ms; animation-delay: 0ms; animation-name: fadeInDown;">
                <img src="images/portfolio/off-road.jpg" class="img-responsive" alt="">
            </div>

        </div>
    </div>
</section>



<?php include_once 'footer.php';?>
